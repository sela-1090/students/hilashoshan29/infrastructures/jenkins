# Install a Jenkins instans with helm


### Create namespace.yaml file:

 apiVersion: v1 
<br> kind: Namespace </br>
<br> metadata: </br>
<br> &emsp;    name: cicd </br>

### Run the next flug to create the cicd namespace

~~~
kubectl create -f namespace.yaml 
~~~

### Adding the Jenkins chart repository

To make this chart repository available, run the following commands:

~~~
helm repo add jenkins https://charts.jenkins.io

helm repo update
~~~

### Config the cicd namespace

To config the cicd namespace instade of the default, run the following commands:

~~~
kubectl config set-context --current --namespace=cicd
~~~

### Deploying a simple Jenkins instance

To deploy a Jenkins instance with the default settings, run the command:

~~~
helm upgrade --install myjenkins jenkins/jenkins
~~~

The first command listed in the notes returns the password for the admin user, the seconde is to run the container:

~~~
kubectl exec --namespace cicd -it svc/myjenkins -c jenkins -- /bin/cat /run/secrets/additional/chart-admin-password

kubectl --namespace default port-forward svc/myjenkins 8080:8080
~~~


### Connect to jankins

go to the link in your cli: 
<br>usename: admin</br>
<br>password: (the output from the previous step)</br> 

## Steps to customize your Jenkins Logo

- Get your avatar logo in gitlab and upload it into the profiles repo
- copy the image address
- Install Simple Theme Plugin in jenkins
- Go to Manage Jenkins → Configure System → Theme → Extra CSS
- paste this code and change the URL to the one you uploaded to GitLab

~~~

/* Custom Jenkins Logo /
.logo {
/ Hide the default logo */
display: none;
}
.logo::before {
/* Set the custom logo image /
content: url("https://gitlab.com/sela-1090/collaboration/profiles/-/blob/main/hilashoshan29/hilashoshan29.jpg?ref_type=heads");
/ Add any additional styles for the logo, if needed /
display: inline-block;
width: 50px; / Adjust the width to fit your logo's dimensions /
height: 50px; / Adjust the height to fit your logo's dimensions */
}
/* Custom Jenkins Name */
#jenkins-home-link {
/* Hide the default text */
display: none;
}
~~~

# How to uninstall a Jenkins instans with helm

~~~
kubectl delete pod myjenkins --namespace cicd

helm uninstall myjenkins --namespace cicd

kubectl delete namespace cicd

helm repo remove jenkins
~~~